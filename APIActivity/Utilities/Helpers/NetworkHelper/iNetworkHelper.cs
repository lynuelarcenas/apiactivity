﻿using System;
using System.Threading.Tasks;

namespace APIActivity.Utilities.Helpers.NetworkHelper
{
    public interface iNetworkHelper
    {
        bool HasInternet();
        Task<bool> IsHostReachable();
    }
}
