﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace APIActivity.Utilities.Helpers.DatabaseReader
{
    public interface iDBReader
    {
        void CreateTable<ClassName>() where ClassName : new();
        Task FetchData<ClassName>(CancellationToken ct) where ClassName : new();
        Task SaveItemAsync<ClassName>(object jsonData, string tableName) where ClassName : new();
    }
}
