﻿using System;
namespace APIActivity.Utilities.Constants
{
    public class APIConstants
    {
        public static string URL = "https://afternoon-bayou-57712.herokuapp.com/";
        public static string VERSION = "v1/";
        public static string STUDENTS_URL = "students/";
        public static string ROOT_URL = URL + VERSION + STUDENTS_URL;
    }
}
