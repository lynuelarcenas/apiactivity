﻿using System;
namespace APIActivity.Models
{
    public class Student
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string name { get; set; }
        public int age { get; set; }
        public int gender { get; set; }
    }
}
