﻿using System;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace APIActivity.Utilities.Helpers.FileReader
{
    public interface iFileConnector
    {
		void ReceiveJSONData(JObject json, CancellationToken ct);
        void ReceiveTimeoutError(int error, string errorStr);
    }
}
