﻿using System;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace APIActivity.Utilities.Helpers.DatabaseReader
{
    public interface iDBConnector
    {
        void ReceiveJSONData(JObject json, CancellationToken ct);
    }
}
