﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace APIActivity.Utilities.Helpers.APIReader
{
    public interface iAPIReader
    {
        Task Get(string id, CancellationToken ct);
        Task Post(JObject json, CancellationToken ct);
        Task Put(JObject json, string id, CancellationToken ct);
        Task Delete(string id, CancellationToken ct);

        //Task Get(string url, CancellationToken ct);
        //Task Post(string url, Object dictionary, CancellationToken ct);
        //Task Put(string url, Object dictionary,  CancellationToken ct);
        //Task Delete(string url, CancellationToken ct);


    }
}
