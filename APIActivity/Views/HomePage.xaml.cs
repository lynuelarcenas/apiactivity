﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using APIActivity.Models;
using APIActivity.Utilities.Helpers.APIReader;
using APIActivity.Utilities.Helpers.DatabaseReader;
using APIActivity.Utilities.Helpers.FileReader;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace APIActivity.Views
{
    public partial class HomePage : ContentPage, iFileConnector, iAPIConnector, iDBConnector
    {
        HttpClient client;
        FileReader fileReader = new FileReader();
        APIReader apiReader = new APIReader();
        DBReader dBReader = DBReader.GetInstance;

        private CancellationTokenSource ct = new CancellationTokenSource();
        private bool isDBCalled = false;

        public HomePage()
        {
            InitializeComponent();
            dBReader.DBReaderDelegate = this;
//#if DEBUG
//            fileReader.FileReaderDelegate = this;
//#else
            apiReader.APIReaderDelegate = this;
//#endif
            //Device.StartTimer(new TimeSpan(0, 0, 5), () =>
            //{
            //    GetData();
            //    return true;
            //});
        }

        void GetData()
        {
            Task.Run(async () =>
            {
//#if DEBUG
//            await fileReader.ReadFile("Student.json", ct.Token);
//#else  
            await apiReader.Get(Utilities.Constants.APIConstants.ROOT_URL, ct.Token); 
//#endif
            });
        }

        async void PostButton_Clicked(object sender, System.EventArgs e)
        {           
            JObject newjson = new JObject();
            newjson.Add("first_name", firstnameEntry.Text);
            newjson.Add("last_name", lastnameEntry.Text);
            //newjson.Add("name", firstnameEntry.Text + " " + lastnameEntry.Text);
            //newjson.Add("course", lastnameEntry.Text);
            newjson.Add("age", ageEntry.Text);
            JObject newnewjson = new JObject();
            newnewjson.Add("student", newjson);
            await apiReader.Post(newnewjson, ct.Token);
            await apiReader.Get(string.Empty, ct.Token);
            ClearEntries();           
        }

        async void PutButton_Clicked(object sender, System.EventArgs e)
        {
            JObject newjson = new JObject();
            newjson.Add("first_name", firstnameEntry.Text);
            newjson.Add("last_name", lastnameEntry.Text);
            //newjson.Add("name", firstnameEntry.Text + " " + lastnameEntry.Text);
            //newjson.Add("course", lastnameEntry.Text);
            newjson.Add("age", ageEntry.Text);
            newjson.Add("gender", 1);

            JObject newnewjson = new JObject();
            newnewjson.Add("student", newjson);
            await apiReader.Put(newnewjson, idEntry.Text, ct.Token);
            await apiReader.Get(string.Empty, ct.Token);
            ClearEntries();
        }

        async void GetButton_Clicked(object sender, System.EventArgs e)
        {
            await apiReader.Get(idEntry.Text, ct.Token);
            listView.IsRefreshing = false;
        }

        async void DeleteButton_Clicked(object sender, System.EventArgs e)
        {
            await apiReader.Delete(idEntry.Text, ct.Token);
            await apiReader.Get(string.Empty, ct.Token);
            ClearEntries();
            //await restService.DeleteDataAsync<Student>(url + Utilities.Constants.STUDENT_URL + "/" + Int32.Parse(IDEntry.Text), ct.Token);
            //await apiReader.Delete(Utilities.Constants.APIConstants.ROOT_URL+Int32.Parse(idEntry.Text), ct.Token);
            await apiReader.Delete(idEntry.Text, ct.Token);
        }

        void ListView_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            string[] varName = ((Models.Student)((ListView)sender).SelectedItem).name.Split(' ');
            firstnameEntry.Text = ((Models.Student)((ListView)sender).SelectedItem).name.Remove(((Models.Student)((ListView)sender).SelectedItem).name.LastIndexOf(' '));
            lastnameEntry.Text = varName[varName.Length - 1];
            ageEntry.Text = ((Models.Student)((ListView)sender).SelectedItem).age.ToString();
            idEntry.Text = ((Models.Student)((ListView)sender).SelectedItem).id.ToString();
        }

        public void ClearEntries()
        {
            idEntry.Text = "";
            firstnameEntry.Text = "";
            lastnameEntry.Text = "";
            ageEntry.Text = "";
        }

        public void ReceiveJSONData(JObject json, CancellationToken ct)
        {
            //Device.BeginInvokeOnMainThread(async () => {
                var students = JsonConvert.DeserializeObject<ObservableCollection<Models.Student>>(json["students"].ToString());
                this.listView.ItemsSource = students;

                //if (json.ContainsKey("student"))
                //{
                //    var std = JsonConvert.DeserializeObject<Models.Student>(json["student"].ToString());
                //    dataFields.BindingContext = std;
                //}
                //if (isDBCalled == false)
                //{
                //    dBReader.CreateTable<Models.Student>();
                //    foreach (var student in students)
                //    {
                //        Console.WriteLine(student.id + " " + student.name + " " + student.age);
                //        await dBReader.SaveItemAsync<Models.Student>(student, "Student");
                //    }
                //}

            //});
           
        }

        public async void ReceiveTimeoutError(int error, string errorStr)
        {
            isDBCalled = true;
            //dBReader.CreateTable<Models.User>();
            await dBReader.FetchData<Models.Student>(ct.Token);
        }
    }
}
