﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace APIActivity.Utilities.Helpers.FileReader
{
    public interface iFileReader
    {
        Task ReadFile(string fileName, CancellationToken ct);
    }
}
