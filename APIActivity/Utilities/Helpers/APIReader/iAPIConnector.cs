﻿using System;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace APIActivity.Utilities.Helpers.APIReader
{
    public interface iAPIConnector
    {
        void ReceiveJSONData(JObject json, CancellationToken ct);
    }
}
