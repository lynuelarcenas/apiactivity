﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace APIActivity.Utilities.Helpers.APIReader
{
    public class APIReader : iAPIReader
    {
        NetworkHelper.NetworkHelper network = NetworkHelper.NetworkHelper.GetInstance;
        HttpClient client;
        const string APIURL = "https://afternoon-bayou-57712.herokuapp.com/v1/students/{0}";
        //const string APIURL = "https://lit-anchorage-34154.herokuapp.com/v1/students/{0}";


        WeakReference<iAPIConnector> _apiDelegate;
        public APIReader()
        {
            client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.MaxResponseContentBufferSize = 256000;
        }

        public iAPIConnector APIReaderDelegate
        {
            get
            {
                iAPIConnector apiDelegate;
                return _apiDelegate.TryGetTarget(out apiDelegate) ? apiDelegate : null;
            }

            set
            {
                _apiDelegate = new WeakReference<iAPIConnector>(value);
            }
        }

        async public Task Get(string id, CancellationToken ct)
        {
            if (network.HasInternet())
            {
                var uri = new Uri(string.Format(APIURL, string.Empty));

                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    var json = JObject.Parse(content);
                    APIReaderDelegate?.ReceiveJSONData(JObject.Parse(content), ct);
                    //Console.WriteLine(json);
                }
            }
            else
            {
                Console.WriteLine("No internet");
            }
        }

        async public Task Post(JObject json, CancellationToken ct)
        {
            var uri = new Uri(string.Format(APIURL, string.Empty));

            var jObject = JsonConvert.SerializeObject(json);
            var content = new StringContent(jObject, Encoding.UTF8, "application/json");

            HttpResponseMessage response = null;
            response = await client.PostAsync(uri, content);
            Console.WriteLine(response);

            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine(response);
            }

            Refresh(APIURL);
        }

        async public Task Put(JObject json, string id, CancellationToken ct)
        {
            var uri = new Uri(string.Format(APIURL, id));

            var jObject = JsonConvert.SerializeObject(json);
            var content = new StringContent(jObject, Encoding.UTF8, "application/json");
            var response = await client.PutAsync(uri, content);
            Console.WriteLine(response);

            Refresh(APIURL);
        }

        async public Task Delete(string id, CancellationToken ct)
        {
            //RestUrl = http://developer.xamarin.com:8081/api/todoitems/{0}
            var uri = new Uri(string.Format(APIURL, id));

            var response = await client.DeleteAsync(uri);
            Console.WriteLine(response);

            Refresh(APIURL);
       }

        async void Refresh(string url)
        {
            CancellationToken ct = new CancellationToken();
            var responses = await client.GetAsync(string.Format(APIURL, string.Empty));
            var content = await responses.Content.ReadAsStringAsync().ConfigureAwait(false);
            APIReaderDelegate?.ReceiveJSONData(JObject.Parse(content), ct);

        }
    }
}
